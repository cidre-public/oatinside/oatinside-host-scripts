Usage of this repository is available in the main [repository](https://gitlab.inria.fr/cidre-public/oatinside/oatinside).

This repository contains:

- [`receive_events.py`](receive_events.py): Python 2 script that listens on localhost port 4242, waiting for a connection (and events) from OATs'inside runtime library.

- [`create_graph_from_event_file.py`](create_graph_from_event_file.py): Python 2 script that generates dot files corresponding to event files. Use the networkx library.

- [`retrieve_conditions_from_dump.py`](retrieve_conditions_from_dump.py): Python 3 script that use the symbolic engine [angr](https://angr.io/) to retrieve the conditions evaluted during a run described by the events and dump files given in argument.

- [`event_proto2.proto`](event_proto2.proto) and [`generate_proto_headers.sh`](generate_proto_headers.sh): respectively, protobuf file that describes the communication protocol between the phone and the hosts and script that generates parsing files for this protocol:
	* [`event_proto2.pb.cc`](event_proto2.pb.cc) and [`event_proto2.pb.h`](event_proto2.pb.h): protobuf generated files used by OATs'inside android runtime (there are already copied in the corresponding repository).
	* [`event_proto2_pb2.py`](event_proto2_pb2.py): protobuf generated files used by other python scripts.
