from collections import defaultdict
import event_proto2_pb2
from google.protobuf.message import DecodeError
from Queue import Queue
from shutil import rmtree
import signal
from socket import socket, AF_INET, SOCK_STREAM, MSG_WAITALL
from struct import unpack, pack
from threading import Thread
import os

# Run "adb reverse tcp:4242 tcp:4242" to bind the local Android socket to

BASE_DIR = "/tmp/oatinside/"
def output_trace(trace):
    # First event is the invoke one or the load one
    _, ev = trace[0]

    method_name = ""
    caller_type = ""
    if ev.HasField("invoke"):
        caller_type = ev.invoke.caller_value.type
        method_name = ev.invoke.method_name
    else:
        caller_type = ev.methodLoad.caller_type
        method_name = ev.methodLoad.method_name

    # Remove 'L' and ';'
    path = os.path.join(BASE_DIR, caller_type[1:-1])
    if not os.path.exists(path):
        os.makedirs(path)
    filepath = os.path.join(path, method_name)

    with open(filepath, 'ab') as f:
        for (data, _) in trace:
            f.write(pack("<i", len(data)))
            f.write(data)

def content_parser(event_queue):
    stack = defaultdict(list)
    while True:
        data = event_queue.get()
        if data == "":
            break

        event = event_proto2_pb2.Event()
        try:
            event.ParseFromString(bytes(data))
            print event
        except DecodeError:
            print "DecodeError with message: '%s'" % " ".join(["%02x" % ord(c) for c in bytes(data)])
            afdasfd
            break

        if event.HasField("catch"):
            nb_skipped_frame = event.catch.nb_skipped_frame
            for _ in xrange(min(len(stack[event.tid]), nb_skipped_frame)):
                trace = stack[event.tid].pop()
                output_trace(trace)

                if stack[event.tid]:
                    trace = stack[event.tid].pop()
                    trace.append((data, event))
                    stack[event.tid].append(trace)

        else:

            if event.HasField("methodLoad"):
                output_trace([(data, event)])
            else:
                if stack[event.tid]:
                    trace = stack[event.tid].pop()
                    trace.append((data, event))
                    stack[event.tid].append(trace)

            if event.HasField("invoke"):
                # Create new trace
                stack[event.tid].append([(data, event)])
            elif event.HasField("ret"):
                if stack[event.tid]:
                    trace = stack[event.tid].pop()
                    output_trace(trace)

                    if stack[event.tid]:
                        trace = stack[event.tid].pop()
                        trace.append((data, event))
                        stack[event.tid].append(trace)

def content_receiver(event_queue):
    s = socket(AF_INET, SOCK_STREAM)
    s.bind(('localhost', 4242))
    s.listen(1)
    conn, addr = s.accept()
    print "[Connected]"

    while 1:
        size = conn.recv(4, MSG_WAITALL)
        if size:
            data = conn.recv(unpack("<i", size)[0], MSG_WAITALL)
            event_queue.put(data)
        else:
            print "[Disconnected]"
            break

    # Dead pill
    event_queue.put("")
    conn.close()

if os.path.exists(BASE_DIR):
    rmtree(BASE_DIR)

event_queue = Queue()
receiver = Thread(target=content_receiver, args=[event_queue])
parser = Thread(target=content_parser, args=[event_queue])

receiver.start()
parser.start()

receiver.join()
parser.join()
