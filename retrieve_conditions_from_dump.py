#!/usr/bin/env python3

import angr
import archinfo
import argparse
import claripy # for testing constraint equality
from collections import namedtuple
import event_proto2_pb2
from struct import pack, unpack
import sys
import os

####################
# Disable warnings #
####################

import logging
logging.getLogger('angr').setLevel('ERROR')
DEBUG=False

##########################################
# Define the debug exploration technique #
##########################################
class DebugExplorationTechnique(angr.ExplorationTechnique):
    def __init__(self, debug, *args, **kwargs):
        super(DebugExplorationTechnique, self).__init__(*args, **kwargs)
        self.debug = debug

    def step(self, simgr, stash='active', **kwargs):
        if self.debug:
            print("\n======================================================================================================")
            for state in simgr.active:
                print("x0=%s\tx1=%s\tx2=%s\tx3=%s" % (state.regs.x0, state.regs.x1, state.regs.x2, state.regs.x3))
                print("x4=%s\tx5=%s\tx6=%s\tx7=%s" % (state.regs.x4, state.regs.x5, state.regs.x6, state.regs.x7))
                print("x8=%s\tx9=%s\tx10=%s\tx11=%s" % (state.regs.x8, state.regs.x9, state.regs.x10, state.regs.x11))
                print("x12=%s\tx13=%s\tx14=%s\tx15=%s" % (state.regs.x12, state.regs.x13, state.regs.x14, state.regs.x15))
                print("x16=%s\tx17=%s\tx18=%s\tx19=%s" % (state.regs.x16, state.regs.x17, state.regs.x18, state.regs.x19))
                print("x20=%s\tx21=%s\tx22=%s\tx23=%s" % (state.regs.x20, state.regs.x21, state.regs.x22, state.regs.x23))
                print("x24=%s\tx25=%s\tx26=%s\tx27=%s" % (state.regs.x24, state.regs.x25, state.regs.x26, state.regs.x27))
                print("x28=%s\tx29=%s\tx30=%s" % (state.regs.x28, state.regs.x29, state.regs.x30))

                print("cc_op=%s\tcc_dep1=%s\tcc_dep2=%s\tcc_ndep=%s" % (state.regs.cc_op,state.regs.cc_dep1,state.regs.cc_dep2,state.regs.cc_ndep))

                print(state.block().pp())
            print("======================================================================================================")

        simgr = simgr.step(stash=stash, **kwargs)
        return simgr

#########################################
# Custom Angr backend to load dump file #
#########################################

class DumpFileBackend(angr.cle.Backend):
    def __init__(self, binary, binary_stream, **kwargs):
        super(DumpFileBackend, self).__init__(binary, binary_stream, **kwargs)

        if self.arch is None:
            raise CLEError("Must specify arch when loading dump file!")

        if self._custom_entry_point is None:
            self._custom_entry_point = 0

        self._entry = self._custom_entry_point
        self._max_addr = 0
        self._min_addr = 2**64

        self._binary_stream.seek(0, 2)
        file_size = self._binary_stream.tell()
        self._binary_stream.seek(0, 0)

        while True:
            if self._binary_stream.tell() >= file_size:
                break

            # Retrieve the header
            header = b""
            header = self._binary_stream.readline()

            # Parse the header
            addrs, rights = header.split()
            addrs = addrs.split(b'-')
            min_addr = int(addrs[0], 16)
            max_addr = int(addrs[1], 16)
            r = True if rights[0] == ord('r') else False
            w = True if rights[1] == ord('w') else False
            x = True if rights[2] == ord('x') else False

            # Load the corresponding segment
            seg_len = max_addr - min_addr
            string = self._binary_stream.read(seg_len)
            if len(string):
                self.memory.add_backer(min_addr, string)
                self._max_addr = max(max_addr, self._max_addr)
                self._min_addr = min(min_addr, self._min_addr)

#####################################################
# Custom Angr plugin to add event struct to a state #
#####################################################

class EventPlugin(angr.SimStatePlugin):
    def __init__(self, events, current_values={}):
        super(EventPlugin, self).__init__()
        self.events = events
        self.current_values = current_values

    @angr.SimStatePlugin.memo
    def copy(self, memo):
        return EventPlugin(self.events, self.current_values)

###########
# Helpers #
###########

# Get size of instruction at addr
def get_instr_size(proj, addr):
    return proj.factory.block(addr).capstone.insns[0].insn.size

# Get first operand of the instruction at addr
def get_first_operand(proj, addr):
    return proj.factory.block(addr).capstone.insns[0].insn.op_str.split(',')[0]

# Get second operand of the instruction at addr
def get_second_operand(proj, addr):
    split = proj.factory.block(addr).capstone.insns[0].insn.op_str.split(',')
    if "[" in split[1] and len(split) > 2:
        return split[1] + "," + split[2]
    else:
        return split[1]

# Get stored address operand of the instruction at addr
def get_stored_address_operand(proj, addr):
    return "".join(proj.factory.block(addr).capstone.insns[0].insn.op_str.split('[')[1:]).split(']')[0]

# Get value of expression that represent reg or mem access
def get_value_from_expr(state, expr):
    expr = expr.strip()
    if expr[0] == "[" and "," in expr:
        reg = getattr(state.regs, expr.split(',')[0].replace('[',''))
        shift = int(expr.split(',')[1].replace(']','').replace('#',''), 0)
        return state.memory.load(reg + shift, 4)
    elif expr[0] == "[":
        reg = getattr(state.regs, expr[1:-1])
        return state.memory.load(reg, 4)
    else:
        return getattr(state.regs, expr)

# Get the number of bit for a value type
def value_type_to_nb_bits(value_type, array_subtype):
    if value_type == 'bo' or value_type == 'by':
        return 8
    if value_type == 'fl' or value_type == 'i32':
        return 32
    if value_type == 'do' or value_type == 'i64' or value_type == 'ui64' or value_type == 'st':
        return 64
    if value_type[0] == "[":
        if array_subtype == True:
            return value_type_to_nb_bits(value_type[1:], array_subtype)
        else:
            64
    return 0

def main():
    ##########
    # Target #
    ##########

    parser = argparse.ArgumentParser(description='Use symbolic execution to retrieve condition events')
    parser.add_argument("dump_file", help="analyzed dump files prefix", type=str)
    parser.add_argument("event", help="analyzed events file", type=str)
    parser.add_argument("address", help="Analyzed method adress in hex format", type=str)
    parser.add_argument("-o", "--output", help="output event file. Default is event file appended with '.symb_exec'", type=str)
    args = parser.parse_args()

    DUMP_FILE = args.dump_file
    EVENT_FILE = args.event
    OUTPUT_EVENT_FILE = args.output if args.output else EVENT_FILE + '.symb_exec'

    #######################################################
    # Compute annotations from events and launch analysis #
    #######################################################

    method_code_offset = int(args.address, 16)
    event_addr_offset = 0x0
    trace = []
    trace_tid = -1
    events_data = []
    events_data_index = -1
    analyzed_method = ""
    dumpId = 0
    cond_events = []
    additional_symb_info = {}
    with open(EVENT_FILE, "rb") as f:
        while True:
            size = f.read(4)
            if not size:
                break
            data = f.read(unpack("<i", size)[0])
            events_data.append(data)
            events_data_index += 1

            event = event_proto2_pb2.Event()
            event.ParseFromString(bytes(data))

            if trace_tid == -1:
                trace_tid = event.tid
            if event.HasField('methodLoad'):
                method_code_offset = event.methodLoad.event_addr_offset + event.methodLoad.method_code_offset
            elif event.HasField('read') or event.HasField('write') or event.HasField('ret') or event.HasField('newObj') or event.HasField('getArray') or event.HasField('lengthArray'):
                addr = event.event_address
                name = value_type = value = event_type = ""
                if event.HasField('read'):
                    value_type = event.read.field_value.WhichOneof('value')
                    value = getattr(event.read.field_value, value_type)
                    event_type = "READ"
                    t = event.read.caller_value.type
                    if t[0] == 'L':
                        t = t[1:-1]
                    name = "%s.%s" % (t, event.read.field_name)
                elif event.HasField('write'):
                    value_type = event.write.new_value.WhichOneof('value')
                    value = getattr(event.write.new_value, value_type)
                    event_type = "WRITE"
                    t = event.write.caller_value.type
                    if t[0] == 'L':
                        t = t[1:-1]
                    name = "%s.%s" % (t, event.write.field_name)
                elif event.HasField('ret'):
                    value_type = event.ret.ret_value.WhichOneof('value')
                    value = getattr(event.ret.ret_value, value_type)
                    event_type = "WRITE"
                    t = value_type
                    if t[0] == 'L':
                        t = t[1:-1]
                    name = "ret_%s" % t # WARNING: the prefix "ret_" is used further
                elif event.HasField('newObj'):
                    value_type = event.newObj.allocated_value.WhichOneof('value')
                    value = getattr(event.newObj.allocated_value, value_type)
                    event_type = "WRITE"
                    t = value_type
                    if t[0] == 'L':
                        t = t[1:-1]
                    name = "new_%s" % t # WARNING: the prefix "new_" is used further
                elif event.HasField('getArray'):
                    elems = event.getArray.elements
                    if len(elems) > 0:
                        value_type = "[" + elems[0].WhichOneof('value')
                        value = [getattr(elem, value_type[1:]) for elem in elems]
                    else:
                        value_type = "[L"
                        value = []
                    event_type = "WRITE"
                    name = "array_%x" % (event.getArray.array_addr)
                elif event.HasField('lengthArray'):
                    value = event.lengthArray.length
                    value_type = "i32"
                    event_type = "WRITE"
                    name = "array_%x.length" % event.lengthArray.array_addr

                nb_bits = value_type_to_nb_bits(value_type, True)

                # addr, symbols name, value, number of bits, index in evnets_data
                trace.append((addr, event_type, name, value, value_type, nb_bits, events_data_index, event))

            elif event.HasField('invoke'):
                if analyzed_method in ["", event.invoke.caller_value.type + event.invoke.method_name]:
                    # The analyzed method is called
                    # If the previous call has to be analyzed, do it
                    if dumpId != 0 :
                        call_symb_exec(DUMP_FILE+str(dumpId), method_code_offset, trace, trace_tid, cond_events, additional_symb_info)
                        trace = []

                    analyzed_method = event.invoke.caller_value.type + event.invoke.method_name
                    dumpId = event.invoke.dump_id
                else:
                    name = event.invoke.method_name
                    value = event.invoke.caller_value.ui64
                    trace.append((event.event_address, "INVOKE", name, value, "V", 64, events_data_index, event))

            elif event.HasField('throw'):
                # TODO
                pass

        # If the previous call has to be analyzed, do it
        if dumpId != 0:
            call_symb_exec(DUMP_FILE+str(dumpId), method_code_offset, trace, trace_tid, cond_events, additional_symb_info)

    ###############################
    # Generate the new event file #
    ###############################

    events_data_index = 0
    with open(OUTPUT_EVENT_FILE, "wb") as f:
        for data in events_data:

            if events_data_index in additional_symb_info:
                data = additional_symb_info[events_data_index].SerializeToString()

            f.write(pack("<i", len(data)))
            f.write(data)

            while True:
                if len(cond_events) > 0:
                    cond = cond_events[0]
                    if events_data_index == cond[0]:
                        data = cond[1].SerializeToString()
                        f.write(pack("<i", len(data)))
                        f.write(data)
                        cond_events = cond_events[1:]
                    else:
                        break
                else:
                    break

            events_data_index += 1

def call_symb_exec(dump_filename, method_code_offset, trace, trace_tid, cond_events, additional_symb_info):
    # Load the dump file
    proj = angr.Project(dump_filename,
                        main_opts={'backend': DumpFileBackend, 'arch': 'aarch64', "base_addr": 0x00000, "force_rebase": True},
                        auto_load_libs=False)

    ############################
    # Setup symbolic execution #
    ############################
    print("Entrypoint: 0x%x" % method_code_offset)
    state = proj.factory.call_state(method_code_offset, add_options={
        angr.options.SYMBOLIC_WRITE_ADDRESSES,
    })

    # Init the special x19 register (tr = thread register)
    # art/runtime/arch/arm64/asm_support_arm64.S
    def hook_libcalls(state):
        print('Skip runtime function')
    class BugFree(angr.SimProcedure):
        def run(self):
            print('Skip runtime function (tr reg)')
            return 42
    state.regs.x19 = state.solver.BVS("thread_register", 64)
    for i in range(300):
        # Compute a new fake runtime method address
        fake_lib_addr = 0x13370000 + i*4
        bigendian_fake_lib_addr =  unpack('>q', pack('<q', fake_lib_addr))[0]

        # Store the fake runtime function address in the thread register
        state.memory.store(state.regs.x19 + i*8, bigendian_fake_lib_addr, 8)

        # Write one instruction (ret) at the beginning of the function
        # to avoid angr to crash when jumping in
        state.memory.store(fake_lib_addr, state.solver.BVV(0xc0035fd6, 4*8))

        # Hook fake runtime function
        state.project.hook_symbol(fake_lib_addr, BugFree())

    ################
    # Setup events #
    ################

    # Define events
    Event = namedtuple('Event', ['addr', 'type', 'size', 'symbols', 'value', 'value_type', 'output_reg', 'events_data_index', 'original_event'])
    events = []

    for (addr, event_type, symb, value, value_type, bits, events_data_index, original_event) in trace:
        if event_type in ["READ", "INVOKE"]:
            if addr: # Addr is None if not part of the project
                size = get_instr_size(proj, addr)
                if event_type == "INVOKE":
                    value_type = 'V'
                else:
                    reg = get_first_operand(proj, addr)
                events.append(Event(addr, event_type, size, state.solver.BVS(symb, bits), state.solver.BVV(value, bits), value_type, reg, events_data_index, original_event))
        elif event_type == "WRITE":
            if addr: # Addr is None if not part of the project
                size = get_instr_size(proj, addr)
                reg = state.regs.x13 # TODO: change this hardcoded value

                # TODO: remove hardcoded naming and reg
                if symb[:4] in ["ret_", "new_"]:
                    if symb[:4] == "ret_" and original_event.ret.ret_value.type == 'V':
                        value_type = 'V'
                    else:
                        reg = 'x0'
                        size = 0
                elif symb[:6] in ["array_"]:
                    reg = 'x0'
                else:
                    reg = 0
                    for r in get_stored_address_operand(proj, addr).split(','):
                        r = r.strip()
                        if r[0] == '#':
                            reg += int(r[1:], 0)
                        else:
                            reg += getattr(state.regs, r)

                # TODO: remaining problems: array of array, array of bytes, ...
                if isinstance(value, list):
                    event_value = [state.solver.BVV(v[0], bits) for v in value]
                else:
                    if value_type == "bo":
                        value = int(value)
                    event_value = state.solver.BVV(value, bits)

                events.append(Event(addr, event_type, size, state.solver.BVS(symb, bits), event_value, value_type, reg, events_data_index, original_event))

    if len(events) == 0:
        print("No events, no need to run")
        sys.exit(0)

    # Set event to the first state
    state.register_plugin('events', EventPlugin(events))

    ###############
    # Setup hooks #
    ###############

    # Set heap plugin needed to allocate data for array events
    state.register_plugin("heap", angr.state_plugins.heap.heap_ptmalloc.SimHeapPTMalloc())

    last_event = []
    # Hook chain function
    def replace_hook(state):
        global last_event

        current_event = state.events.events.pop(0)
        original_event = current_event.original_event
        additional_symb_info[current_event.events_data_index] = original_event
        last_event = current_event

        print("@%X event %s" % (current_event.addr, current_event.type))

        if current_event.value_type != "V":

            # TODO: remaining problems: array of array, array of bytes, ...
            if isinstance(current_event.value, list):
                elem_size = value_type_to_nb_bits(current_event.value_type, True)
                nb_elem = len(current_event.value)
                ptr = state.heap.malloc(nb_elem * elem_size)

                setattr(state.regs, current_event.output_reg, ptr)

                new_symb = state.solver.BVS("_".join(str(current_event.symbols._encoded_name).split('_')[:-2]), elem_size*len(current_event.value))
                state.memory.store(ptr, new_symb)
                full_value = state.solver.BVV(0, 0)
                i = 0
                for v in current_event.value:
                    full_value = claripy.ZeroExt(8, full_value)  << 8
                    full_value += claripy.ZeroExt(8*i, v)
                    i += 1
                state.events.current_values[new_symb] = full_value

            elif isinstance(current_event.output_reg, str):
                setattr(state.regs, current_event.output_reg, current_event.symbols)
                state.events.current_values[current_event.symbols] = current_event.value

            else:
                state.memory.store(current_event.output_reg, current_event.symbols, len(current_event.symbols)//8)
                state.events.current_values[current_event.symbols] = current_event.value

            # Update original_event to get the defined symbol
            if original_event.HasField("read"):
                original_event.read.field_value.symb = str(current_event.symbols)
            elif original_event.HasField("write"):
                original_event.write.new_value.symb = str(current_event.symbols)
            elif original_event.HasField("ret"):
                original_event.ret.ret_value.symb = str(current_event.symbols)
            elif original_event.HasField("newObj"):
                original_event.newObj.allocated_value.symb = str(current_event.symbols)

        # Check if getted register/memory aera are symbolic, in order
        # to get the symbol
        if original_event.HasField("read"):
            print(state.project.factory.block(state.solver.eval(state.ip)).capstone.insns[0])
            op = get_second_operand(state.project, state.solver.eval(state.ip))
            value = get_value_from_expr(state, op)
            if value.symbolic:
                if "UNINITIALIZED" not in str(value):
                  original_event.read.field_value.symb = str(value)
        elif original_event.HasField("write"):
            print(state.project.factory.block(state.solver.eval(state.ip)).capstone.insns[0])

            first_op = get_first_operand(state.project, state.solver.eval(state.ip))
            first_op_reg = getattr(state.regs, first_op)
            if first_op_reg.symbolic:
                if "UNINITIALIZED" not in str(first_op_reg):
                  original_event.write.new_value.symb = str(first_op_reg)

            op = get_second_operand(state.project, state.solver.eval(state.ip))
            value = get_value_from_expr(state, op)
            if value.symbolic:
                if "UNINITIALIZED" not in str(value):
                  original_event.write.old_value.symb = str(value)
        elif original_event.HasField("invoke"):
            # TODO: add mapping when high number of arg (mapping to the stack)
            mapping = {0:state.regs.x2,1:state.regs.x3,2:state.regs.x4,3:state.regs.x5,4:state.regs.x6,5:state.regs.x7,6:state.regs.x8,7:state.regs.x9}
            for i in range(len(current_event.original_event.invoke.args)):
                value = mapping[i]
                if value.symbolic:
                    if "UNINITIALIZED" not in str(value):
                        original_event.invoke.args[i].symb = str(value)

        state.project.unhook(current_event.addr)
        if len(state.events.events) > 0:
            next_event = state.events.events[0]
            state.project.hook(next_event.addr, hook=replace_hook, length=next_event.size)

    # Setup hook_chain
    state.project.hook(events[0].addr, hook=replace_hook, length=events[0].size)

    ####################################
    # Define the exploration technique #
    ####################################

    class EventExplorationTechnique(angr.ExplorationTechnique):
        def setup(self, simgr):
            simgr.populate('unfollowed', []) # create the 'unfollowed' stash
            simgr.populate('temp_cond', []) # temporary stash for condition logging

        def step(self, simgr, stash='active', **kwargs):

            simgr.stash(filter_func=lambda st: st.block().size==0, from_stash='active', to_stash='deadended')
            if len(simgr.active) == 0:
                return simgr

            last_addr_of_prev_block = simgr.active[0].block().instruction_addrs[-1]
            simgr.step(stash=stash, **kwargs)

            concreate_constraints = [s == v for s,v in state.events.current_values.items()]
            def test_not_satisfiable(st):
                st2 = st.copy()
                for c in concreate_constraints:
                    st2.solver.add(c)
                return not st2.solver.satisfiable()
            simgr.stash(filter_func=test_not_satisfiable, from_stash='active', to_stash='temp_cond')

            if len(simgr.active) > 1:
                print("The concreate values are not enough: we still have two successor states!")
                print(concreate_constraints)
                sys.exit(-1)

            if len(simgr.temp_cond) and len(simgr.active) == 1:
                claripyTrue = claripy.ast.bool.BoolV(True)
                def exclude_identical_constraints(l1, l2):
                    return [x for x in l1 if list(filter(lambda y: (x==y) is claripyTrue, l2)) == []]

                active = simgr.active[0]
                for s in simgr.temp_cond:
                    global last_event
                    print("@%X branch: %s" % (last_addr_of_prev_block, exclude_identical_constraints(active.solver.constraints, s.solver.constraints)))

                    # Print here potential solutions for the oppposite condition
                    solv = s.solver
                    specific_constraints = exclude_identical_constraints(s.solver.constraints, active.solver.constraints)
                    solv_vars = sum([list(c.variables) for c in specific_constraints], [])
                    solv_symb = [a for a in specific_constraints[0].recursive_leaf_asts if a.symbolic]
                    print("\tsolutions :")
                    for symb in solv_symb:
                        print("\t\t", hex(solv.eval(symb)))

                    cond_event = event_proto2_pb2.Event()
                    cond_event.tid = trace_tid
                    cond_event.event_address = last_addr_of_prev_block
                    cond_event.condition.condition_value = " && ".join([str(e)[6:-1] for e in exclude_identical_constraints(active.solver.constraints, s.solver.constraints)])
                    cond_event.condition.target_address = s.addr

                    cond_events.append((last_event.events_data_index, cond_event))

            simgr.stash(from_stash='temp_cond', to_stash='unfollowed')

            return simgr

    ###########################################################
    # Define a concretization strategy that uses trace values #
    ###########################################################

    class SimConcretizationStrategyTrace(angr.concretization_strategies.SimConcretizationStrategy):
        def _concretize(self, memory, addr):
            print("Concretizing %s " % addr)
            # for s in memory.state.events.current_values.keys() => For all symbol s that have a value set by the trace
            # addr.variables.intersection(s.variables) => get all symbols that are both in addr and in trace value
            # len(...) > 0 => True if there is at least one symbols of addr that is concerned by the trace value
            # True in {len(...)} => check if there is at least one trace value that set one value of the addr
            if True in {len(addr.variables.intersection(s.variables)) > 0 for s in memory.state.events.current_values.keys()}:
                # Add trace value to the solver
                concreate_constraints = [s == v for s,v in memory.state.events.current_values.items()]
                st2 = memory.state.copy()
                for c in concreate_constraints:
                    st2.solver.add(c)
                # If the value is unique (set by the trace)
                if st2.solver.unique(addr):
                    # Return it
                    return [st2.solver.eval(addr)]
    state.memory.write_strategies.insert(0, SimConcretizationStrategyTrace())
    state.memory.read_strategies.insert(0, SimConcretizationStrategyTrace())

    #############################
    # Launch symbolic execution #
    #############################

    # while True:
    # Get a simulation manager
    simgr = proj.factory.simgr(state, techniques=[DebugExplorationTechnique(debug=DEBUG), EventExplorationTechnique()])

    # Run the symbolic execution
    remaining_simgr = simgr.run()

    if len(remaining_simgr.deadended) != 1:
        print("Error len(remaining_simgr.deadended) != 1")
        sys.exit(-1)

    remaining_events = remaining_simgr.deadended[0].events.events

    if len(remaining_events) == 0:
        print("No remaining events")

if __name__ == "__main__":
    main()
