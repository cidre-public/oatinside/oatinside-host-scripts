import argparse
import event_proto2_pb2
import networkx as nx
from networkx.drawing.nx_agraph import write_dot
from struct import unpack
from sys import argv
import sys

MAX_COND_LENGTH=100

ROOT_NODE = -1

parser = argparse.ArgumentParser(description='Create CFG from event file')
parser.add_argument("event", help="analyzed event file", type=str)
parser.add_argument("-o", "--output", help="output event file", default="/tmp/test.dot", type=str)
parser.add_argument("--full", help="Do not filter executions that don't create new nodes", action="store_true")
args = parser.parse_args()

filename = args.event
output_filename = args.output

keptTraces = set()
if not args.full:
    sys.stdout.write("Filter nodes ...")
    sys.stdout.flush()

    alreadySeenReturnValues = set()
    alreadySeenEdges = set()
    newReturnValueInThisTrace = False
    newEdgeInThisTrace = False
    nb_invoke_ret = 0
    nb_traces = 1
    previous_node = ROOT_NODE
    with open(filename, "rb") as f:
        while True:
            size = f.read(4)
            if not size:
                break
            data = f.read(unpack("<i", size)[0])

            event = event_proto2_pb2.Event()
            event.ParseFromString(bytes(data))

            if not event.HasField('methodLoad'):
                if event.HasField('invoke') and nb_invoke_ret==0:
                    addr = 0
                else:
                    addr = event.event_address

                if (previous_node, addr) not in alreadySeenEdges:
                    if not newEdgeInThisTrace:
                        keptTraces.add(nb_traces)
                    newEdgeInThisTrace = True
                    alreadySeenEdges.add((previous_node, addr))

                if event.HasField('ret'):
                    ret_value = str(getattr(event.ret.ret_value, event.ret.ret_value.WhichOneof('value')))
                    if ret_value not in alreadySeenReturnValues:
                        if not newEdgeInThisTrace and not newReturnValueInThisTrace:
                            keptTraces.add(nb_traces)
                        newReturnValueInThisTrace = True
                        alreadySeenReturnValues.add(ret_value)



                previous_node = addr

            if event.HasField('invoke'):
                nb_invoke_ret += 1
            elif event.HasField('ret') or event.HasField('throw'):
                nb_invoke_ret -= 1

            if nb_invoke_ret == 0:
                previous_node = ROOT_NODE
                nb_traces += 1
                newEdgeInThisTrace = False
                newReturnValueInThisTrace = False

    sys.stdout.write(" OK\n")
    sys.stdout.flush()


sys.stdout.write("Create unmerged graph ...")
sys.stdout.flush()

# Create unmerged graph
g = nx.DiGraph()
g.add_node(ROOT_NODE)
previous_node = ROOT_NODE
nodes = {}
nb_invoke_ret = 0
nb_traces = 1
with open(filename, "rb") as f:
    while True:
        size = f.read(4)
        if not size:
            break
        data = f.read(unpack("<i", size)[0])

        event = event_proto2_pb2.Event()
        event.ParseFromString(bytes(data))

        if event.HasField('methodLoad'):
            pass
        elif event.HasField('condition'):
            if args.full or nb_traces in keptTraces:
                addr = event.event_address
                cond_val = event.condition.condition_value[:MAX_COND_LENGTH]
                target = event.condition.target_address

                cond0 = "%x_cond0" % addr
                cond1 = "%x_cond1" % addr
                if cond0 in g.nodes:
                    last_cond0_label = ""
                    last_cond1_label = ""
                    if (previous_node, cond0) in g.edges:
                        last_cond0_label = g[previous_node][cond0]['label']
                        last_cond1_label = g[previous_node][cond1]['label']

                    if g.nodes[cond0]['target'] == target:
                        last_cond0_label += "\nEXEC %i - %s" % (nb_traces, cond_val)
                        g.add_edge(previous_node, cond0, label=last_cond0_label)
                        g.add_edge(previous_node, cond1, label=last_cond1_label)
                        previous_node = cond0
                    else:
                        last_cond1_label += "\nEXEC %i - %s" % (nb_traces, cond_val)
                        g.add_edge(previous_node, cond0, label=last_cond0_label)
                        g.add_edge(previous_node, cond1, label=last_cond1_label)
                        previous_node = cond1
                else:
                    g.add_node(cond0, target=target)
                    g.add_node(cond1, target=-1)

                    g.add_edge(previous_node, cond0, label="\nEXEC %i - %s" % (nb_traces, cond_val))
                    g.add_edge(previous_node, cond1, label="")

                    previous_node = cond0
        else:
            if args.full or nb_traces in keptTraces:
                if event.HasField('invoke') and nb_invoke_ret==0:
                    addr = 0
                else:
                    addr = event.event_address
                nodes[addr] = nodes.get(addr, []) + [(nb_traces, event)]

                g.add_node(addr)
                g.add_edge(previous_node, addr)
                previous_node = addr

            if event.HasField('invoke'):
                nb_invoke_ret += 1
            elif event.HasField('ret') or event.HasField('throw'):
                nb_invoke_ret -= 1
                if nb_invoke_ret == 0:
                    previous_node = ROOT_NODE
                    nb_traces += 1

sys.stdout.write(" OK\n")

sys.stdout.write("Remove useless blank ...")
sys.stdout.flush()

# Remove useless blank node
for addr in list(g.nodes):
    if isinstance(addr, basestring): # If blank node
        if len(g[addr]) == 1: # If only one next node
            next_node = g[addr].keys()[0]
            if not isinstance(next_node, basestring):
                for prev_node in g.predecessors(addr):
                    g.add_edge(prev_node, next_node, label=g[prev_node][addr]['label'])
                g.remove_node(addr)

sys.stdout.write(" OK\n")

sys.stdout.write("Create label mapping ...")
sys.stdout.flush()

# Change label for each node

def beautiful_type(t):
    if t == "D":
        return "double"
    elif t == "F":
        return "float"
    elif t == "J":
        return "long"
    elif t == "I":
        return "int"
    elif t == "S":
        return "short"
    elif t == "Z":
        return "boolean"
    elif t == "B":
        return "byte"
    elif t == "C":
        return "char"
    elif t.startswith("L"):
        return t[1:-1]
    elif t.startswith("["):
        return beautiful_type(t[1:]) + "[]"

mapping = {}
nb_blank_node = 0
for addr in g.nodes:
    if isinstance(addr, basestring):
        nb_blank_node += 1
        s = "blank node no %i" % nb_blank_node
    elif addr == ROOT_NODE:
        s = "ROOT NODE"
    else:
        s = "%x:" % addr
        for nb_traces, ev in nodes[addr]:
            s += "\tEXEC %i - %s " % (nb_traces, ev.WhichOneof('event_data').upper())
            if ev.HasField('read'):
                v = getattr(ev.read.field_value, ev.read.field_value.WhichOneof('value'))
                t = ev.read.caller_value.type
                if t[0] == 'L':
                    t = t[1:-1].split('/')[-1]
                if ev.read.field_value.type == 'Ljava/lang/String;':
                    v = "\"%s\"" % v
                elif ev.read.field_value.type[0] in ['[', 'L']:
                    v = "0x%x" % v
                v_self = getattr(ev.read.caller_value, ev.read.caller_value.WhichOneof('value'))
                s += "(%s 0x%x).%s => %s" % (t, v_self, ev.read.field_name, str(v))
                if ev.read.field_value.symb:
                    s += " #%s" % ev.read.field_value.symb
            elif ev.HasField('write'):
                t = ev.write.caller_value.type
                if t[0] == 'L':
                    t = t[1:-1].split('/')[-1]
                v_self = getattr(ev.write.caller_value, ev.write.caller_value.WhichOneof('value'))
                v = getattr(ev.write.new_value, ev.write.new_value.WhichOneof('value'))
                s += "(%s 0x%x).%s <= %s" % (t, v_self, ev.write.field_name, str(v))
                if ev.write.new_value.symb:
                    s += " #%s" % ev.write.new_value.symb
            elif ev.HasField('invoke'):
                t = ev.invoke.caller_value.type
                if t[0] == 'L':
                    t = t[1:-1].split('/')[-1]
                v_self = getattr(ev.invoke.caller_value, ev.invoke.caller_value.WhichOneof('value'))
                args_v = ", ".join(["(%s)%s %s" %
                                    (beautiful_type(a.type),
                                     "\"%s\"" % a.st if a.type == 'Ljava/lang/String;' else
                                     "0x%x" % getattr(a, a.WhichOneof('value'))
                                     if a.type[0] == 'L' or a.type[0] == '[' else
                                     str(getattr(a,
                                                 a.WhichOneof('value'))), a.symb)
                                    for a in ev.invoke.args])
                s += "(%s 0x%x).%s(%s)" % (t, v_self, ev.invoke.method_name, args_v)
            elif ev.HasField('ret'):
                s += " " + str(getattr(ev.ret.ret_value, ev.ret.ret_value.WhichOneof('value')))
                if ev.ret.ret_value.symb:
                    s += " #%s" % ev.ret.ret_value.symb
            elif ev.HasField('condition'):
                s += "%s -> %x" % (ev.condition.condition_value, ev.condition.target_address)
            elif ev.HasField('throw'):
                s += "%s: \"%s\"" % (ev.throw.exception_type, ev.throw.exception_text)
            elif ev.HasField('newObj'):
                s += "%s => 0x%x" % (ev.newObj.allocated_value.type, ev.newObj.allocated_value.ui64)
                if ev.newObj.allocated_value.symb:
                    s += " #%s" % ev.newObj.allocated_value.symb
            elif ev.HasField('getArray'):
                s += "0x%x => [%s]" % (ev.getArray.array_addr, ", ".join(str(getattr(element, element.WhichOneof('value'))) for element in ev.getArray.elements))
            elif ev.HasField('lengthArray'):
                s += "0x%x => %i" % (ev.lengthArray.array_addr, ev.lengthArray.length)
            s += "\n"
    mapping[addr] = s

sys.stdout.write(" OK\n")

sys.stdout.write("Apply mapping ...")
sys.stdout.flush()

nx.relabel_nodes(g, mapping, copy=False)

sys.stdout.write(" OK\n")

sys.stdout.write("Output ...")
sys.stdout.flush()

# Output dot
write_dot(g, output_filename)

sys.stdout.write(" OK\n")
sys.stdout.flush()
